local wibox = require("wibox")  -- Provides the widgets
local watch = require("awful.widget.watch")
local awful = require("awful")
-- Create the text widget
local count_windows_text = wibox.widget{
    font = "RobotoMonoNerdFont 8",
    widget = wibox.widget.textbox,
}
-- Create the background widget
local count_windows = wibox.widget.background()
count_windows:set_widget(count_windows_text) -- Put the text inside of it
count_windows_text:set_text("{0}")

client.connect_signal('focus', function ()
    count_windows_text:set_text("{"..#awful.screen.focused().clients.."}")
end)

return count_windows