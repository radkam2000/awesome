#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

run "nm-applet"
run "setxkbmap pl"
run "volumeicon"
run "xrandr --output DP-4 --primary --left-of HDMI-0"